(function Main( win,doc ) {
	//Class declaration
	var CanvasHelper = function ( canvas_el ) {
		var context = canvas_el.getContext("2d");
    context.imageSmoothingEnabled = false;

    this.putImage = function (params) {
      context.drawImage.apply(context, params );
      return this;
    }

    this.draw = function () {
      // for (var x = 0.5; x < 500; x += 10) {
      //   context.moveTo(x, 0);
      //   context.lineTo(x, 375);
      // }

      // context.strokeStyle = "#000";
      // context.stroke();
    },
    this.getImageData = function () {
      return context.getImageData(0,0,300,150);
    }

    this.putImageData = function (data, where_x, where_x) {
      return context.putImageData(data,where_x,where_x);
    }
	};

	var Jukebox = function (audio_element,tracks) {
		var audio_el = audio_element;
		var tracks_arr = tracks;
		var curr_track_idx = 0;
		this.changeTrack = function (shufle) {
			var order = shufle || false;
			//@todo Shuffle tracks
			if (!order) {
				if (tracks_arr.length) {
					if ( this.getCurrTrack() == tracks_arr.length - 1 ) {
						curr_track_idx = 0;
					} else {
						curr_track_idx++;
					}
					var src = tracks_arr[curr_track_idx];
					audio_el.src = src;
				}
			}
		}
		this.getCurrTrack = function () {
			return curr_track_idx;
		}
	};

  //Data
	var answers = ["No.", "Nope", "Never", "Not yet","ううん"];
	var sound_files = ["resources/under_the_moon.mp3", "resources/under_the_moon_classic.mp3"];

	var index = Math.floor( Math.random() * 5 );
	doc.getElementById("answer").innerText = answers[index];

  win.addEventListener("load",function () {
    var canvas = doc.getElementById("canvas-moon");
    var moon = doc.getElementById("moon");
    moon.crossOrigin = "Anonymous"
    var cvHelper = new CanvasHelper(canvas).putImage([moon,0,0,300,150]);
    // var imgData = cvHelper.getImageData();

    // var size = imgData.data.length;
    // console.log(size)
    //var index = 3; //Alpha channel start index

    // for (var i = 0; i < size; i++) {
    //   if (i != 0 ) {
    //     index += 4;
    //   }

    //   if (imgData.data[index] > 0 ) {
    //     imgData.data[index];
    //   }
    // };

    // cvHelper.putImageData(imgData, 0,0);
  });
	doc.addEventListener("keyup",function (e) {
		var key = e.keyCode;
		if (key === 32) {
			music_time.changeTrack();
		}
	});

	var audio_el = doc.getElementById("audio");
	var music_time = new Jukebox(audio_el, sound_files);

})(this, this.document);
